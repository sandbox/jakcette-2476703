; ci_audit_tools make file for d.o. usage
core = "7.x"
api = "2"

; +++++ Modules +++++

projects[ctools][version] = "1.6"
projects[ctools][subdir] = "contrib"

projects[date][version] = "2.8"
projects[date][subdir] = "contrib"

projects[profiler_builder][version] = "1.2"
projects[profiler_builder][subdir] = "contrib"

projects[feeds][version] = "2.0-alpha8"
projects[feeds][subdir] = "contrib"

projects[feeds_jsonpath_parser][version] = "1.0-beta2"
projects[feeds_jsonpath_parser][subdir] = "contrib"

projects[feeds_xpathparser][version] = "1.0"
projects[feeds_xpathparser][subdir] = "contrib"

projects[entityreference][version] = "1.1"
projects[entityreference][subdir] = "contrib"

projects[field_group][version] = "1.4"
projects[field_group][subdir] = "contrib"

projects[link][version] = "1.3"
projects[link][subdir] = "contrib"

projects[ldap][version] = "2.0-beta8"
projects[ldap][subdir] = "contrib"

projects[advanced_help][version] = "1.1"
projects[advanced_help][subdir] = "contrib"

projects[entity][version] = "1.5"
projects[entity][subdir] = "contrib"

projects[job_scheduler][version] = "2.0-alpha3"
projects[job_scheduler][subdir] = "contrib"

projects[json2][version] = "1.1"
projects[json2][subdir] = "contrib"

projects[libraries][version] = "2.2"
projects[libraries][subdir] = "contrib"

projects[pathauto][version] = "1.2"
projects[pathauto][subdir] = "contrib"

projects[revisioning][version] = "1.9"
projects[revisioning][subdir] = "contrib"

projects[token][version] = "1.5"
projects[token][subdir] = "contrib"

projects[rules][version] = "2.8"
projects[rules][subdir] = "contrib"

projects[shib_auth][version] = "4.1"
projects[shib_auth][subdir] = "contrib"

projects[term_merge][version] = "1.2"
projects[term_merge][subdir] = "contrib"

projects[views][version] = "3.10"
projects[views][subdir] = "contrib"

projects[views_bulk_operations][version] = "3.2"
projects[views_bulk_operations][subdir] = "contrib"

projects[views_exposed_groups][version] = "1.0"
projects[views_exposed_groups][subdir] = "contrib"

